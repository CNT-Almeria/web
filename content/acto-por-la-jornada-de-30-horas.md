Title: Acto por la jornada de 30 horas
Date: 2017-10-17 11:37
Tags: 30, campaña, coloquio, horas, jornada, reducción, salarial, tiempo
Category: Eventos
Slug: acto-por-la-jornada-de-30-horas
Image: images/2017/cartel-acto-30-horas.jpg

Desde <abbr title="Confederación Nacional del Trabajo-Asociación
Internacional de los Trabajadores">CNT-AIT</abbr> os invitamos <time datetime="2017-10-19T20:00">este próximo jueves 19 de octubre a las 20:00h</time> a la presentación que estamos llevando a cabo por las 30 horas de jornada laboral.

Se realizará en nuestro local. Después del coloquio habrá un cenador para
la autogestión de nuestro sindicato.
