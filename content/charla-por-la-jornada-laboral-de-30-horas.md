Title: Charla por la jornada laboral de 30 horas
Date: 2014-12-03
Tags: actividades, destacado
Category: Eventos
Slug: charla-por-la-jornada-laboral-de-30-horas
Image: images/2014/relojes-30-horas.jpg

Con motivo de la campaña que la CNT-AIT Almería está llevando a cabo
durante los meses de noviembre y dicembre por una la jornada laboral de
30 horas, realizaremos una charla-debate el martes, 9 de diciembre de
2014, a las 20:00h, en el local de La Oficina Producciones (Calle de las
Tiendas 26, 1º).

Os esperamos!
