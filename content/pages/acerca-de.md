Title: Quiénes somos
Date: 2017-06-27 00:29
Slug: quienes-somos
Image: images/2017/527px-Anarchist_black_cat.svg.png

Pertenecemos al sindicato de oficios varios de la <abbr
title="Confederación Nacional del Trabajo-Asociación Internacional de
los Trabajadores">CNT-AIT</abbr>. Desde esta federación practicamos el
[anarcosindicalismo](https://es.wikipedia.org/wiki/Anarcosindicalismo),
luchando contra la explotación laboral y construyendo una sociedad libre
y sin opresión desde abajo.

Para realizar nuestros objetivos de forma independiente no aceptamos
subvenciones del Estado, manteniendo el sindicato mediante la
autogestión (dinero recaudado en eventos culturales, las cuotas de las
personas afiliadas al sindicato...).

Si buscas un sindicato independiente, sin jerarquía, que luche
incansablemente por los derechos y la libertad de las trabajadoras,
acompáñanos en la lucha. Únete a la <abbr>CNT</abbr>, [contacta con
nosotras](http://www.cntaitalmeria.es/pages/contacto.html).
