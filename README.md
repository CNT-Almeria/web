## Dependencias

Puedes generar este blog en tu ordenador. Para ello necesitas tener los
siguientes programas instalados:
* [Pelican](https://blog.getpelican.com/). Es el generador de páginas
* [Python](http://python.org/). Es el lenguaje de programación en el que
  está escrito Pelican
* [Markdown](https://pypi.python.org/pypi/Markdown/). Markdown es el
  lenguaje de marcado ligero en el que están escritos los artículos y
  páginas

## Instrucciones de instalación

### GNU/Linux y Mac

GNU/Linux y Mac OS suelen traer Python instalado por defecto. Para
instalar los requisitos de Python en estos sistemas operativos basta con
ejecutar la siguiente instrucción: `pip install markdown pelican`.

### Windows

Para Windows hace falta descargar e instalar Python usando un
ejecutable ofrecido en su [página web oficial](https://www.python.org/downloads/) y después ejecutar `python -m pip install markdown pelican`.

## Tema de la web

El tema de la web es personalizado, pero se creó a partir de [Pelican
Clean Blog](https://github.com/gilsondev/pelican-clean-blog). Por tanto
comparte la misma licencia: [Expat](https://gitlab.com/CNT-Almeria/sitio-web/blob/master/COPYING).

## Licencia

El contenido se encuentra bajo la licencia [Creative Commons Attribution
4.0 International License](http://creativecommons.org/licenses/by/4.0/).

El código fuente se encuentra bajo la licencia
<a href="https://gitlab.com/CNT-Almeria/web/blob/master/COPYING" rel="license"><abbr title="General Public License version 3">GPLv3</abbr></a>,
salvo donde se indique lo contrario (siempre bajo otras licencias de
software libre).
