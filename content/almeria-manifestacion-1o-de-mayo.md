Title: Manifestación 1º de mayo
Date: 2015-04-22 16:48
Category: Eventos
Tags: 1º de mayo, Almería

Día a día estamos viendo como las reformas laborales han conseguido que
la precariedad se convierta en esa amiga inseparable. Bajada de
salarios, aumento de horas de trabajo... Por no hablar de todas esas
personas que no tenemos ni salario, ni trabajo o de aquellas que se ven
obligadas a marcharse en busca de un "futuro".

Paro, precariedad y explotación se han convertido en una forma de vida
que nos está condenando a la miseria.

**El 1º de mayo es una fecha histórica, que nos recuerda que la lucha sí
sirve para algo. Por ello, desde CNT-Almería os animamos a participar en
la manifestación que realizaremos e iniciaremos en el Mercado de los
Ángeles a las 12:00h.** No sólo os animamos a participar en la
manifestación sino, a que hagáis de la lucha un comportamiento cotidiano
para no aceptar el chantaje, las situaciones injustas y empezar a
controlar de nuevo el destino de nuestra propia vida.

Hay alternativa, el cambio es posible. Desde CNT planteamos medidas como
la jornada semanal generalizada de 30 horas, como motor de mejora de las
condiciones y calidad de vida, además de suponer una herramienta en
contra del desempleo.

De ti depende que esta vida de mierda se convierta en algo que merezca
la pena vivirla.

**MANIFESTACIÓN 1º DE MAYO, 12:00h. MERCADO DE LOS ÁNGELES.**
