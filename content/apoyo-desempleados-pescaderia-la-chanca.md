Title:  La CNT-AIT de Almería apoya a los desempleados de Pescadería-La Chanca
Category: Eventos
Date: 2014-05-21 18:56
Tags: desempleados, pescadería
Image: images/2014/pescaderia_3.jpg

Desde la CNT-AIT de Almería queremos expresar nuestro apoyo a la próxima
convocatoria de la Plataforma de Desempleados de Pescadería-La Chanca
contra los cortes de agua a las familias sin recursos, por  viviendas
dignas y por un trabajo digno para las clases trabajadoras de los
barrios populares.

Así mismo, queremos animar a que en cada barrio de la ciudad  los
trabajadores continúen organizándose de forma asamblearia, poniendo en
común  sus  problemas  y buscando soluciones  formas de lucha colectiva.

Animamos también a todos los desempleados y trabajadores, así como a
nuestra militancia a acompañar a los vecinos de este barrio en esta
convocatoria.

Día 23 de Mayo, plaza de la Constitución a las 11 Horas.  Desde el
Ayuntamiento hasta las oficinas de Aqualia

¡Por unos barrios dignos para la clase trabajadora!
