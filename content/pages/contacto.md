Title: Contacto
Date: 2017-07-21 00:45
Modified: 2017-09-19 11:49
Slug: contacto

<dl>
    <dt>Sede del Sindicato</dt><dd>Javier Sanz 14, 3º 04003 ALMERIA</dd>
    <dt>Dirección postal</dt><dd>Apartado 1098. 04080 ALMERIA</dd>
    <dt>Teléfono y Fax</dt><dd>950 62 18 62</dd>
    <dt>Correo-e</dt><dd>sov ARROBA cntaitalmeria.es</dd>
</dl>

<a href="http://www.cntaitalmeria.es/images/2017/local-sindicato.png">
    <img alt="El local de la CNT Se encuentra en frente del Instituto de Educación Secundaria Celia Viñas en Almería capital" class="img-responsive" src="http://www.cntaitalmeria.es/images/2017/local-sindicato.png">
</a>

Abrimos de lunes a jueves desde las <time>18:30</time> hasta las
<time>20:30</time>.
