Title: nosotrxs elegimos la #abstenciónactiva
Date: 2015-03-09 20:20
Category: Opinión
Tags: Almería, anarquismo, vídeo
Image: images/2015/video-abstencion-activa.png

¿Cansada de ver cómo el político de turno sigue manejando tu vida? ¿de
ver el futuro negro, muy negro?

¡Pásate a la abstención activa! Piensa, organízate y lucha.

En las próximas citas electorales celebremos el triunfo de la abstención activa y el comienzo de la recuperación del control de nuestra propia vida.

¡Compañerxs!

El futuro es nuestro.
