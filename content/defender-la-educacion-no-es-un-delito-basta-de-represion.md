Title: Defender la educación no es un delito. ¡Basta de represión!
Date: 2013-10-03 18:03
Category: Opinión
Tags: Almería, enseñanza
Image: images/2013/defender-la-educacion-no-es-un-delito.jpg

En la pasada huelga en el sector de la educación, estudiantes y
trabajadores salieron a la calle para luchar contra la inminente
privatización de la educación, para parar la LOMCE y pedir mejoras y
reclamar derechos en las condiciones laborales.

La respuesta del Estado a través de la Subdelegación del Gobierno y sus
instrumentos de represión (Policía Nacional) ha sido la de sancionar
económicamente a través de una veintena de multas a aquellas personas que
participaron en la convocatoria con el afán de defender los derechos de la
clase trabajadora y el acceso y permanencia al sistema educativo en igualdad de
oportunidades. Cinco de esas multas fueron a parar a militantes de este
sindicato que decidieron secundar la convocatoria. <!-- more --> Lo
paradójico de esta historia es que tanto a estas cinco multas como al
resto se les acusa de acciones y hechos que no acontecieron y además
fueron impuestas sin identificación previa ni levantamiento de acta de
denuncia delante de los acusados. Empieza a oler a podrido...

La historia más llamativa es la de un joven estudiante que pertenece a
otro colectivo y que participa activamente en los movimientos sociales
de la capital. Éste también recibió la multa a pesar de que se
encontraba a miles de kilómetros disfrutando de la conocida beca
Erasmus. ¿Hablamos del uso de listas negras por parte de la Brigada de
Información de la Policía Nacional? ¡Toma democracia!... ¡come libertad!

Lo que está claro es que parte de esas multas han ido a parar a personas que
participan activamente en los movimientos sociales o militan en algún colectivo
u organización política de índole contraria a la que sustenta el poder. Estas
medidas impuestas tienen un claro sentido amedrantador ya que lo que busca el
Estado con las mismas es perseguir, intimidar, atemorizar y apaciguar a
personas dispuestas a salir a la calle a luchar por lo que les pertenece y a
todo aquél que de un paso adelante y peleen por sus derechos y los derechos de
los que vienen detrás. Pero el Estado se olvida de una herramienta potente con
la que cuenta la clase trabajadora: la solidaridad. Estas multas no van a
frenar nuestra acción sindical y política. Seguiremos luchando en los centros
de estudio, trabajo y en la calle contra el sistema capitalista y contra el
Estado que nos oprime. Esta campaña se hace con el objetivo de visibilizar los
tejemanejes que la democracia usa para tenernos “atados y bien atados” y
denunciar públicamente el acoso al que se ven sometidos los movimientos
sociales en esta capital y el resto del estado español. DEFENDER LA EDUCACIÓN
NO ES UN DELITO CONTRA LA REPRESIÓN: ACCIÓN DIRECTA, SOLIDARIDAD Y LUCHA
POPULAR

CNT-AIT SECCIÓN ENSEÑANZA ALMERÍA

![Defender la educación no es un delito. ¡Basta de represión!](http://www.cntaitalmeria.es/images/2013/defender-la-educacion-no-es-un-delito-cnt-ait.jpg)
