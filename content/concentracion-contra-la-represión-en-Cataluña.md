Title: Concentración contra la represión en Cataluña
Date: 2017-10-03 09:18
Modified: 2017-10-03 18:16
Tags: Cataluña, concentración, huelga, huelga general, represión
Category: Eventos
Image: images/2017/apoyo-huelga-3-octubre-cataluña-cnt.jpg

<del datetime="2017-10-03 18:15Z">Hoy, día 3 de octubre, se realizará una concentración a las
<time datetime="19:30">19:30</time> en Puerta Puchena.
Se trata de un apoyo a la huelga general que se celebra en Cataluña,
contra la represión que sufrieron las personas durante el
referéndum.</del>

<ins datetime="2017-10-03 18:15Z">Finalmente, no nos han dado los permisos
necesarios para convocar la manifestación, y se ha decidido
desconvocarla.</ins>
