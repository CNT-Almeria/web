Title: Concentración por la muerte de Carlos Palomino
Date: 2017-11-09 15:10
Tags: antifascismo, concentración, Palomino
Category: Eventos
Image: images/2017/concentracion-palomino.jpg

Mañana, viernes 10 de noviembre a las 20:00, se va a celebrar una
concentración en la Rambla a la altura de Obispo Orberá por el asesinato
de Carlos Palomino a manos de un nazi, del cual hacen ya 10 años.

La convocatoria está organizada por la [Plataforma Antifascista de
Almería](http://almeriaantifa.blogspot.co.uk/), en la que CNT participa.
