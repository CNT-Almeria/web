Title: MANIFESTACIÓN 1º DE MAYO ALMERÍA. 12:00. MERCADO DE LOS ÁNGELES
Date: 2013-04-25 19:33
Category: Eventos
Tags: 1º de mayo, 1º de mayo, Almería, laboral

Fecha y hora del evento: 
<span class="date-display-start">26/04/2013 (Todo el día)</span><span
class="date-display-separator"> -</span><span
class="date-display-end">02/05/2013 (Todo el día)</span>

1º de mayo 2013. Rompamos con su juego. Construyamos la alternativa.
(manifestación)

**Este primero de mayo de 2013, la lucha continúa.**

**MANIFESTACIÓN 1ºDE MAYO 2013. 12:00h. MERCADO DE LOS ÁNGELES.**

**Terminada la manifestación habrá un mitin-fiesta en la carpa Juan Goytisolo.**
