Title: Segunda visita a la Delegación de Empleo por el conflicto en Proilabal
Date: 2013-07-25 18:41
Category: Noticias
Tags: Almería, anarcosindicalismo, laboral
Image: images/2013/conflicto-proilabal-1.jpg

Nos hemos vuelto a concentrar en las instalaciones de la Delegación de Empleo
de la Junta de Andalucía para seguir protestando por el despido del compañero.
En contra de la represión sindical y por la readmisión del mismo. Recordamos
que esta empresa despidió a nuestro compañero (delegado de la sección sindical
de CNT), tras las demandas hechas por éste para mejorar las condiciones
laborales en la empresa.

<!-- more -->

Este despido ha sido avalado por la delegada de UGT, que para colmo
es la responsable del departamento de recursos humanos de la empresa.

En todo momento los transeúntes e incluso trabajadores de empleo se
han acercado interesados por el tema a mostrarnos su apoyo. No ha
ocurrido ningún incidente.

**¡Proilabal escucha! ¡La CNT sigue en lucha!**

**¡Basta de represión sindical!**

**READMISIÓN COMPAÑERO DESPEDIDO**

![Imagen de la concentración](http://www.cntaitalmeria.es/images/2013/conflicto-proilabal-1.jpg)
![Imagen de la concentración](http://www.cntaitalmeria.es/images/2013/conflicto-proilabal-2.jpg)
