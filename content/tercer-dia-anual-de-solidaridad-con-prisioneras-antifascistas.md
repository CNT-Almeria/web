Title: Tercer dia anual de solidaridad con prisioneras antifascistas
Date: 2017-07-25 19:30
Tags: antifascismo, solidaridad
Category: Noticias
Slug: tercer-dia-anual-de-solidaridad-con-prisioneras-antifascistas
Image: images/2017/3-anyo-solidaridad.jpg

En este tercer año de solidaridad con prisioneras antifascistas queremos
recordar la injusticia que padecen compañeros y compañeras antifascistas
de todo el mundo, y exigir la amnistía total a las condenadas por luchar
contra el fascismo y el capitalismo, que tanto van de la mano.

Este día de solidaridad nació en 2014 a raíz de que Jock Palfreeman
fuera sentenciado a 20 años de cárcel por defender a dos personas
gitanas de violentas hinchas fascistas.
