Title: Manifestación por la jornada laboral de las 30 horas (20 diciembre)
Date: 2014-12-11
Tags: convocatorias, destacado
Category: Eventos
Slug: manifestacion-20-diciembre-30-horas-anuncio
Image: images/2014/manifestacion30_horas.jpg


**MANIFESTACIÓN POR LAS 30 HORAS**

**20 DE DICIEMBRE, A LAS 12:00H,**

**SALIDA: PLAZA DEL QUEMADERO**

**OS ESPERAMOS!**

<em><u>Por las 30 horas de jornada laboral</u></em>

<em><u>Sin reducción de salario y sin aumento de productividad</u></em>

<em><u>Por el reparto del trabajo, por el reparto de la riqueza</u></em>

“Hay una guerra de clases, pero es mi clase, la de los ricos, quien está haciendo la guerra y la estamos ganando.”

Warren Buffett. Multimillonario Americano
