Title: Charla sobre represión en el Rif
Date: 2017-11-13 20:25
Tags: charla, represión, Rif
Category: Eventos
Image: images/2017/RIF.jpg

El 18 de noviembre de 2017 a las 19:00h se realizará una charla sobre la
represión en el Rif en el local de CNT Almería.

Existe un conflicto muy grave en el Rif, entre su población y el
gobierno de Marruecos. Esta región, a nivel histórico, ha tenido
conflictos tanto con el estado marroquí como con el estado español.
Actualmente esta población se organiza en torno al Movimiento Popular
Rifeño (MPR) para luchar contra la represión que sufren y a favor de su
libertad. 

En la charla nos contarán este conflicto de primera mano. Después de la
charla habrá un cenador.
