Title: 2 de diciembre antifranquista
Date: 2017-12-01 22:29
Tags: antifascismo, cenador, documental, franquismo, plataforma antifascista, transición
Category: Eventos
Image: images/2017/antifranquismo-evento.jpg

Este sábado 2 de diciembre a partir de las 19:00, la Plataforma
Antifascista de Almería organiza una charla y una proyección de un
documental en la sede de CNT-AIT Almería (C/ Javier Sanz nº14 3ºD).

La charla se titula *La historia de la transición española contada por
gente de la calle*, y el documental es *No se os puede dejar solos*.
Solo se proyectará la primera parte del documental. Tras el evento habrá
un cenador.
