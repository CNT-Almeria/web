Title: Acto antifascista 12 de octubre
Date: 2017-10-12 00:24
Tags: Almería, antifascismo, convivencia
Category: Eventos
Image: images/2017/evento-antifascista-octubre.jpg

Hoy, 12 de octubre desde las <time datetime="12:00">12:00h</time> hasta las <time datetime="17:00">17:00h</time>,  se va a realizar un acto en el Paseo Marítimo, frente a la
sede del <abbr title="Partido Comunista de España">PCE</abbr>.
La organiza Almería Antifascista, plataforma de la que la <abbr
title="Confederación Nacional del Trabajo">CNT</abbr>
forma parte.

Habrá convivencia, música, bebida y mucho más. ¡Venid, lo pasaremos bien!
