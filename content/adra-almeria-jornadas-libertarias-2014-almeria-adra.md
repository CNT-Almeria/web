Title: Jornadas libertarias 2014 Almería-Adra
Category: Eventos
Date: 2014-04-14 18:49
Tags: Almería, anarquismo, cultura
Image: images/2014/jornadas-libertarias-2014-almeria-adra.jpg

Vueve la primavera y con ello las Jornadas Libertarias de la CNT-AIT.
Este año no sólo se podrá disfrutar de la cultura libertaria en la
localidad almeriense, sino que se extenderá a la provincia,
concretamente a Adra y El Ejido.

Este año trataremos la lucha vecinal de la mano de los propios vecinos
del barrio de "Gamonal" que participaron en el conflicto. El aborto
libre, seguro y gratuíto. coincidiendo con las elecciones europeas
hablaremos de abstención activa, teatro, exposición, conciertos...

<!-- more -->

Esperamos vuestra participación.

PROGRAMACIÓN:

DEL 22 DE ABRIL AL 9 DE MAYO

“Viviendo la Utopía”. Exposición de carteles y fotografías de la
revolución social española 1936-1939.Centro cultural de Adra (Almería).
De 17 a 22h.

29 DE ABRIL

Conferencia: Abuelos no os olvidamos. La desmemoria pública del
antifascismo:Ponente: Jorge Ramos. Historiador e investigador de la
Universitat de Valencia.Salón de actos de la CNT-AIT de Adra. 19.30h.

1º DE MAYO

Manifestación. Salida mercado de los Ángeles (Almería). 12:00h.

3 DE MAYO

Charla: Conflicto en el barrio burgalés de El Gamonal: Lucha vecinal que
pretende prender la llama.Salón de actos CNT-AIT de Almería.19:30h.

9 DE MAYO

Charla: Por los derechos reproductivos: aborto libre, seguro y
gratuito.Salón de actos de CNT-AIT de Almería. 19:30h.

10 DE MAYO

Torneo de fútbol antirracista con comida popular (Adra).

13 DE MAYO

Proyección: El retorno de los Weichafe. Los procesos de recuperación
territorial mapuche.Coloquio con el autor Gonzalo Mateos.La oficina de
producciones culturales, Almería. 19:00h.

15 DE MAYO

Teatro: En la plaza de mi pueblo. De títeres desde abajo.En la Rambla a
la altura del IES Celia Viñas. 19:30 h.

17 DE MAYO

Concierto: Mentenguerra + Kronstadt. Salón de actos de la CNT-AIT de
Adra. 21:30h.

20 DE MAYO

Conferencia: La abstención activa. Porque no votamos los
anarquistas.Ponente: José Luis García Rúa. Catedrático de filosofía en
la universidad de Granada.Salón de actos de la escuela de adultos de El
Ejido. 19:00h.

24 DE MAYO

Concierto: Brutu’s daugthers + Carne + Llorssais + The lagers + Komo
kome la kaballa. Sala Juan Goytisolo (Almería). 22:00h.

27 DE MAYO

Proyección: “Búsqueda piquetera”. Sobre la recuperación de las fábricas
en Argentina.Presentacion posterior con Guillermo, militante
patagónico.Salón de actos de la CNT-AIT de Adra. 19.30h.

30 DE MAYO

Charla: MADRES CONTRA LA IMPUNIDAD :Un caso de represión contra los
movimientos sociales pro-presxs.Salón de actos de la CNT-AIT de Almería.
19:30h.

31 DE MAYO

Charla: Experiencias vitales de unxs supervivientes a la crisis. Corrala
El Triunfo (Granada).Salón de actos de la CNT-AIT de Almería. 19:30h

![Programa de las jornadas libertarias de Almería, 2014.](http://www.cntaitalmeria.es/images/2014/programa-jornadas-libertarias-almeria-2014.jpg)
