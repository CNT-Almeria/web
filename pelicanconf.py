#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals


# Basic settings (http://docs.getpelican.com/en/stable/settings.html#basic-settings)
DEFAULT_CATEGORY = 'Sin categoría'
DISPLAY_CATEGORIES_ON_MENU = True
DISPLAY_PAGES_ON_MENU = True
PATH = 'content'
PLUGIN_PATHS = ['plugins']
PLUGINS = ['another_read_more_link']
SITENAME = 'CNT-AIT Almería'
SITEURL = 'http://www.cntaitalmeria.es'

# URL settings (http://docs.getpelican.com/en/stable/settings.html#url-settings)
RELATIVE_URLS = True

# Time and date (http://docs.getpelican.com/en/stable/settings.html#time-and-date)
TIMEZONE = 'Europe/Madrid'
LOCALE = ('es_ES.UTF-8')

# Metadata (http://docs.getpelican.com/en/stable/settings.html#metadata)
AUTHOR = 'CNT Almería'

# Feed settings (http://docs.getpelican.com/en/stable/settings.html#feed-settings)
# feed generation is usually not desired when developing, set to true in publishconf.py
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Pagination (http://docs.getpelican.com/en/stable/settings.html#pagination)
DEFAULT_PAGINATION = 6

# Translations (http://docs.getpelican.com/en/stable/settings.html#translations)
DEFAULT_LANG = 'es'
TRANSLATION_FEED_ATOM = None

# Themes (http://docs.getpelican.com/en/stable/settings.html#themes)
THEME = 'theme'
LINKS = (('CNT', 'http://cnt.es/'),)

DIASPORA_URL = 'https://dorf-post.de/people/860e5dd0a1f80135fa760ab9c178c99c'
FACEBOOK_URL = 'https://www.facebook.com/SOVAlmeria'
MASTODON_URL = 'https://hostux.social/@cntaitalmeria'
TWITTER_URL = 'https://twitter.com/CNTAITalmeria'
RADDLE_URL = 'https://raddle.me/f/Anarquismo'

DISPLAY_CATEGORIES_ON_MENU = True

# Plugins' configuration (not from Pelican core)
ANOTHER_READ_MORE_LINK = 'Continúa leyendo <span class="screen-reader-text">{title}</span>'
ANOTHER_READ_MORE_LINK_FORMAT = ' <a class="more-link" href="{url}#read_more_link">{text}</a>'
