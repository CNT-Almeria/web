Title: Por la jornada laboral de 30 horas semanales
Category: Opinión
Date: 2017-07-21 06:00
Tags: 30 horas, COA, laboral, tiempo
Image: images/2017/30-horas.jpg

Desde CNT-AIT Almería, junto a <abbr title="Coordinadora Obrera Anarquista">COA</abbr>,
lanzamos una campaña sobre la jornada laboral de 30 horas con tres objetivos
claros: acabar con el paro, las jornadas a destajo y mejorar nuestra vida.

¿Cuántos jóvenes han tenido que emigrar por sueldos de miseria con contratos
que no llegan a 4 horas pero la jornada real llega a sobre pasar las 12 horas?

¿Cuántas familias siguen resistiendo la crisis, esa que dicen que ha acabado,
medigando a padres o abuelos?

La reducción de la jornada, sin reducción salarial y, por supuesto, sin aumento
de la productiviad, es una reivindicación con el objetivo de acabar con el
desempleo de unos y la precariedad de otros.

<!-- more -->

Trabajar menos para trabajar todos. Y, a su vez, es una reivindicación pensada
para disfrutar de nuestras vidas, sometidas a los lazos del trabajo
interminable o a la búsqueda desesperada de éste.

Esta propuesta no es el fin a nuestros males. Pero sí es un modo de comenzar a
librarnos de los lastres del trabajo asalariado (ese que hacemos para otros y
se queda con el producto que elaboramos) y el desempleo (la necesidad que crea
este sistema de mendigar ese trabajo asalariado). Así, de algún modo, nos
daremos cuenta de que la vida es algo más que trabajar.

<video controls src="http://www.cntaitalmeria.es/video/2017/Por_las_30_horas_semanales.mp4">Lo siento, para poder ver el vídeo necesitas un navegador compatible con HTML 5 o superior</video>
