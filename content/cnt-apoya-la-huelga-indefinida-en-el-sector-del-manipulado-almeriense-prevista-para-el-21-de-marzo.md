Title: CNT apoya la huelga indefinida en el sector del manipulado almeriense prevista para el 21 de marzo
Date: 2016-03-18 09:08
Category: Noticias
Tags: Almería, convenios, huelga, laboral
Image: images/2016/sector-manipulado.jpg

- **La intransigencia de la patronal no deja otra salida que la huelga**

- **Han equiparado la condiciones laborales del sector a la de los
  países del sudeste asiático**

La Confederación Nacional del Trabajo (CNT) manifiesta públicamente su
apoyo a la Huelga Indefinida en el Sector del Manipulado de Almería
convocada por CCOO y UGT a partir del día 21 de marzo. La central
anarcosindicalista, con una modesta  presencia en el sector del
manipulado almeriense, estima, sin embargo, que de ninguna manera se
debe permitir el chantaje de la patronal y las dilaciones en la
negociación colectiva para forzar un convenio aun más miserable a cambio
de no perder la ultraactividad del actual convenio.

<!-- more -->

La patronal del sector, de una manera mayoritaria vulnera el convenio
vigente así como el Estatuto de los Trabajadores, jornadas de hasta 14,
15 o 16 horas sin respetar el descanso mínimo entre jornadas; en muchos
almacenes no se está indemnizando a las trabajadoras eventuales
contratadas bajo la modalidad de fin de obra o servicio con los 12 días
de salario por año trabajado establecidos para cuando terminan sus
contratos; paulatinamente, se ha despedido personal fijo o fijo
discontinuo para sustituirlo por personal eventual aumentando
arbitrariamente la precariedad en el sector. 

Es una vergüenza las condiciones denigrantes en las que trabajan tanto
las envasadoras como el resto de personal en algunos almacenes;
condiciones que no son propias de países de nuestro entorno y que se
explican por la cultura de "nuevo rico" de cooperativistas y patronos
preocupados exclusivamente, en aumentar sus plusvalías a costa del
sufrimiento y la explotación de los aproximadamente 30.000 trabajadoras
y trabajadores, nativos e inmigrantes, que con su esfuerzo diario están
sacando a flote un sector importantísimo de la economía almeriense sin
que nadie se lo reconozca. 

Según datos del Ministerio de Economía y Competitividad publicados
recientemente en la prensa, Almería exportó 3.122 millones de euros,
siendo la provincia que más creció de toda la región, representa el
12,5% del total de las ventas de la comunidad autónoma andaluza. Estas
cifras están vinculadas a que durante el año 2015 volvió a marcarse un
récord en exportaciones agroalimentaria con un 5,5% más que en 2014.

Para CNT es inaceptable que un sector económico en auge, mantenga unos
salarios que han perdido en los últimos años hasta un 7% de poder
adquisitivo, tampoco olvidamos que, se ha aprovechado, las sucesivas
reformas laborales que han facilitado el despido, para amedrentar a las
plantillas imponiendo unas condiciones laborales por debajo del
 convenio colectivo. 

Desde la CNT animamos a todas las trabajadoras y trabajadores del
manipulado a sumarse a la huelga indefinida, ya que sólo con la lucha
unitaria y medidas de acción directa que afecten económicamente a las
empresas, podremos lograr que valoren nuestro trabajo diario y
reconozcan nuestros derechos. Pero también la CNT exige transparencia y
horizontalidad en las negociaciones, es indispensable la participación
de las trabajadoras y trabajadores en la ratificación de cualquier
acuerdo ya sea previo a la huelga o en el transcurso de esta. Las
dimensiones del sector hacen complicado un proceso asambleario pero no
es imposible, ejemplos hay muchos. En CNT consideramos que cualquier
acuerdo que se alcance debe ser ratificado en asamblea para evitar
cambalaches como en la convocatoria de huelga de 2012 donde se rubricó
un convenio alejado de las expectativas iniciales. 

Por otra parte animamos a la sociedad almeriense para que se sume a las
diversas iniciativas como por ejemplo la recogida de firmas en pro de un
convenio digno para el sector o en la participación activa en las
manifestaciones convocadas o en los piquetes informativos en los días de
huelga. De una u otra forma todos estamos vinculados con este sector, o
porque trabajamos en él o porque lo hacen familiares cercanos. Ya es
hora de decir basta.   

Por un convenio digno para el sector del manipulado

**¡El 21 de marzo huelga indefinida!**

![Trabajadores en una de las empresas de manipulado de productos agrícolas.](http://www.cntaitalmeria.es/images/2016/trabajadores-en-una-de-las-empresas-de-manipulado-de-productos-agricolas.jpg)
