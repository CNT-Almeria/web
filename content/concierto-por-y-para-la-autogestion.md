Title: Concierto por y para la autogestion: ¡Hazlo tú misma!
Date: 2017-11-07 15:13
Tags: autogestión, concierto, Murcia
Category: Eventos
Image: images/2017/concierto-retal.jpg

El 11 de noviembre a las 20:00h celebraremos un concierto por y para la
autogestión en el <a href="https://csoaelretal.wordpress.com/"><abbr title="Centro Social Okupado
Anarquista">CSOA</abbr> El Retal</a> de la provincia de Murcia.

Tocarán los siguientes grupos:

- [Sabotaje](https://sabotajepunk.bandcamp.com/album/demo-s-t) (Murcia)
- No-Do (Almería)
- [Pasto Nefasto](https://pastonefasto.bandcamp.com/album/derrapando-y-sin-frenos) (Almería)
- La Bilis Negra (Almería)
- DJ Inútil (Almería)

La dirección es Avenida Miguel de Cervantes 114, Murcia. Toca al timbre
si es necesario.
