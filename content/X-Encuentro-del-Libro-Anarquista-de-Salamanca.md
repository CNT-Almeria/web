Title: X encuentro del libro anarquista de salamanca
Date: 2017-07-31 21:28
Tags: anarquismo, cultura, libro
Category: Eventos
Slug: x-encuentro-del-libro-anarquista-de-salamanca
Image: images/2017/x-encuentro-libro.jpg

Los días 11 y 12 de agosto de 2017 se celebra el *X Encuentro del Libro
Anarquista de Salamanca*.

Animamos a todas las compañeras de Almería que quieran acudir a que
organicen el viaje juntas. Se puede encontrar toda la información en la
[página web del evento](http://encuentrosalamanca.blogspot.com.es/).
