Title: Manifestacion 20 diciembre (crónica)
Date: 2014-12-20
Tags: convocatorias, destacado
Category: Eventos
Slug: manifestacion-20-diciembre-30-horas

Este 20 de diciembre se realizó una manifestación que puso fin a una
campaña llevada cabo durante dos meses (noviembre-diciembre) en la
capital almeriense, por la implantación de la jornada laboral de 30
horas, sin reducción salarial. La campaña ha consistido en la
realización de una serie de charlas y acciones encaminadas a la difusión
de esta reivindicación histórica llevada a cabo por la Confederación
Nacional del Trabajo (CNT).

<!-- more -->

<video controls src="http://www.cntaitalmeria.es/video/2014/Manifestaci%C3%B3n_por_la_jornada_laboral_de_30_horas_(20_12_2014).mp4">Lo siento, para poder ver el vídeo necesitas un
navegador compatible con HTML 5 o superior</video>
