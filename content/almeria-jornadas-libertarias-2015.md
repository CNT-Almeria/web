Title: Jornadas Libertarias 2015
Category: Eventos
Date: 2015-04-13 18:55
Tags: Almería, anarquismo, cultura
Image: images/2015/cartel-jornadas-libertarias-2015.jpg

**Ya están aquí las Jornadas Libertarias (2015) de la CNT-AIT Almería.**

Este año, hablaremos y debatiremos sobre feminismo, autodefensa digital,
abstención activa, represión, y nos acercaremos al movimiento de
liberación kurdo y a la resistencia palestina.

<!-- more -->

Las ideas anarquistas siguen estando vivas (por mucho que las den por
muertas) y seguirán siendo una constante para la transformación social.
¡Vive lo libertario! ¡Acércate a las Jornadas Libertarias! ¡Acércate a la
CNT-AIT!

Todos los actos se realizarán en el local de la CNT Almería (C/Javier
Sanz, nº14, 3ºdrch, frente al IES Celia Viñas) y comenzarán a las
20:00h exceptuando los del 16 y 22 de mayo que se realizarán en 
LaOficina Producciones Culturales y la manifestación del 1º de mayo
que tendrá lugar en la Plaza del Mercado de los Ángeles a las 12:00h.

¡Os esperamos!

<a href="http://www.cntaitalmeria.es/images/2015/cartel-jornadas-libertarias-2015.jpg">
<img alt"Cartel de las jornadas libertarias de 2015" src="http://www.cntaitalmeria.es/images/2015/cartel-jornadas-libertarias-2015.jpg">
</a>
