Title: Jornadas Libertarias CNT-AIT Almería 2013 (crónica)
Category: Eventos
Date: 2013-04-29 18:27
Tags: Almería, anarquismo, cultura, vídeo
Image: images/2013/logo-jornadas-libertarias-2013.jpg

Acabadas las jornadas, nos queda el buen sabor de boca tanto por la notable participacipación como por la organización de los distintos actos ya que todos discurrieron con total normalidad. Aquí os dejamos un pequeño resumen-crónica de lo que fueron. Damos las gracias a todxs lxs que participasteis.Hasta la próxima primavera! "La anarquía no es imposible,
es necesaria".
