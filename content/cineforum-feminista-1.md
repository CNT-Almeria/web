Title: Cineforum feminista 1: Las memorias de Antonia
Date: 2017-10-21 18:03
Tags: cine, cineforum, debate, discusión, feminismo, memorias
Category: Eventos
Image: images/2017/Memorias_de_Antonia.jpeg

El <time datetime="2017-10-26T20:00">jueves 26 de octubre a las
20:00h</time> inauguraremos el ciclo de cine antipatrialcal con la
proyección de *Memorias de Antonia*. Después de la película realizaremos
una pequeña discusión sobre los temas tratados en ella.

El objetivo de esta actividad (a la que queremos dar continuidad) es
disfrutar de películas a la vez que fomentamos el espíritu crítico y el
debate. Pásate por el local de <abbr title="Confederación Nacional del
Trabajo">CNT</abbr> y acompáñanos durante la película. ¡Nosotras
invitamos a palomitas!
