Title: Jornadas libertarias 2013
Category: Eventos
Date: 2013-04-01 18:10
Tags: Almería, anarquismo, cultura
Image: images/2013/logo-jornadas-libertarias-2013.jpg

La CNT-AIT Almería vuelve este abril de 2013 con sus jornadas para crear debate
y poner sobre la mesa el punto de vista de las ideas libertarias. Con charlas
como las que nos ofrecerán José Luis García Rúa o Mujeres Libres del Aljarafe. 
Este año el deporte también tiene un papel importante ya que por primer año se
realiza un torneo de fútbol (odio al fútbol moderno) y nos dejaremos los pies
en esa jornada de senderismo, con la subida al Chullo. Por último, hemos
invitado de nuevo a participar en nuestras jornadas a la banda madrileña de
ska: “Skainhead” y a otras de la provincia (LLorssais, Los Tarbinas, Dirty
Reason) para una noche de fiesta y diversión que promete.

<!-- more -->

Esperamos vuestra participación.

*“La anarquía no es imposible, es necesaria”.*

ACTIVIDADES (ABRIL 2013):

<ul>
    <li>
    <strong>Día 6</strong>. Torneo de fútbol. (interesadxs contactar sovalmeria@cnt.es).
    </li>
    <li>
    <strong>Día 12</strong>. “La lucha social hecha por mujeres: proyecto de autogestión obrera”.
    Por Mujeres Libres del Aljarafe. En la Oficina Producciones a las 20:00.
    </li>
    <li>
    <strong>Día 17</strong>. “Procesos constituyentes ¿para qué?”. Por José Luis García Rúa.
    En la Universidad de Almería (Aula Magna humanidades) a las 12:00.
    </li>
    <li>
    <strong>Día 20</strong>. Concierto: Skainhead, LLorssais, Los Tarbinas, Dirty Reason.
    Carpa Juan Goytisolo, aperturas de puertas a las 20:00h, inicio de los
    conciertos a las 21:00h. Entrada Gratuita.
    </li>
    <li>
    <strong>Día 25 y 26</strong>. Fin de semana de senderismo; subida al Chullo. (interesadxs contactar sovalmeria@cnt.es)
    </li>
</ul>

![Imagen del programa de las jornadas libertarias](http://www.cntaitalmeria.es/images/2013/programa-de-las-jornadas-libertarias.jpg)
![Concierto 20/04/2013. Entrada gratuita. Carpa Juan Goytisolo. Apertura de puertas 20:00, inicio de conciertos 21:00, comida, bebida... Jornadas Libertarias](http://www.cntaitalmeria.es/images/2013/jornadas-libertarias-concierto.jpg)
