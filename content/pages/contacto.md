Title: Contacto
Date: 2017-07-21 00:45
Modified: 2017-08-08 19:49
Slug: contacto

<dl>
    <dt>Sede del Sindicato</dt><dd>Javier Sanz 14, 3º 04003 ALMERIA</dd>
    <dt>Dirección postal</dt><dd>Apartado 1098. 04080 ALMERIA</dd>
    <dt>Teléfono y Fax</dt><dd>950 62 18 62</dd>
    <dt>Correo-e</dt><dd>almeria ARROBA autistici.org</dd>
</dl>

<a href="http://www.cntaitalmeria.es/images/2017/local-sindicato.png">
    <img alt="El local de la CNT Se encuentra en frente del Instituto de Educación Secundaria Celia Viñas en Almería capital" class="img-responsive" src="http://www.cntaitalmeria.es/images/2017/local-sindicato.png">
</a>
