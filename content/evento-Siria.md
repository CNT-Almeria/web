Title: Evento 2 de septiembre sobre conflicto sirio
Date: 2017-08-30 20:07
Tags: documental, guerra, refugiadas, revolución, Siria
Category: Eventos
Image: images/2017/evento-siria.jpg

El próximo 2 de septiembre se realizará un evento dedicado a la guerra
de Siria, que comenzará a las <time datetime="19:30">19:30h</time> en el
salón de actos de la <abbr title="Confederación Nacional del
Trabajo-Asociación Internacional de los Trabajadores">CNT-AIT</abbr> Almería:

* Se proyectará el documental *Voces desde Siria*.
* Firás Fansa dará una charla titulada «El origen de la revolución en
  Siria y el desarrollo del conflicto».
* Una voluntaria en un campo de refugiados en Grecia nos contara su
  experiencia de vivir allí.
