Title: Jornadas libertarias 2014 Adra-Almería (vídeo-resumen)
Category: Eventos
Date: 2014-06-23 20:58
Tags: jornadas, libertarias, video

Aquí, un pequeño recuerdo de lo que fueron las Jornadas Libertarias 2014
Adra-Almería.

Queremos dar las gracias a los/as compañeros/as y en general, a todas
las personas que hicieron posible la realización de todas las
actividades.

Ahora sólo nos toca esperar a que venga la primavera...

<video controls src="http://www.cntaitalmeria.es/video/2014/Resumen_Jornadas_Libertarias_2014.mp4">Lo siento, para poder ver el vídeo necesitas un navegador compatible con HTML 5 o superior</video>
