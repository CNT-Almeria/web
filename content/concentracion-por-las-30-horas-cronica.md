Title: Concentración por las 30 horas (crónica)
Category: Eventos
Date: 2014-12-01
Tags: convocatorias, destacado
Image: images/2014/cronica-concentracion-30-horas.jpg

El sábado 29 de Noviembre  y dentro de la campaña por las 30 H,
compañer@s este sindicato y del SOV de Adra nos hemos concentrado en la
Plaza de Pavía, en el popular barrio de Pescadería-La Chanca.

La elección del barrio no es casual, dentro de los elevados porcentajes
de desempleo dentro de Andalucía, este  barrio destaca alcanzando un
desempleo  mayor al 60 %. Tras desplegar una pancarta hemos repartido
unos panfletos en los que se explica la demanda de una jornada laboral
de 30 horas, sin reducción salarial ni aumento de la productividad.
Durante este reparto son bastantes los y las trabajadoras del barrio que
se han ido acercando para informarse de nuestra campaña, expresarnos
apoyo  y consultarnos dudas que les surgían sobre la campaña. Así mismo,
hemos informado de los próximos actos que desde este SOV tenemos
programados continuando la campaña por las 30 horas.

El día  9 de Diciembre a las 20 H, en la Oficina Producciones culturales
en la c/ las Tiendas compañer@s del SOV ofrecerán una charla sobre esta
campaña reivindicativa y como colofón, el día 20 de Diciembre la las
12H, desde la plaza del Quemadero convocamos una manifestación con el
mismo lema, tras esta tendremos un tapeo popular para tod@s l@s que
querías  acompañarnos.

Os esperamos.
