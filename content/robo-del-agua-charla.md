Title: Robo del agua: Acuafundios en Almería
Date: 2017-09-23 15:19
Tags: acuíferos, agua, cenador, charla, debate, ecología, explotación, exposición, fotografía, libros, folletos
Category: Eventos
Image: images/2017/rioaguas-charla.jpg

*Acuafundios*. Hemos necesitado inventar un nueva palabra para describir
lo que está pasando en la provincia de Almería. Si quieres saber más, te
invitamos a participar a esta charla-debate que se realizará el 28 de
septiembre a las <time datetime="20:00">20:00</time> en el local de
<abbr title="Confederación Nacional del Trabajo">CNT</abbr> Almería.

El primer ponente, José María Calaforra, docente de la Universidad de
Almería y Presidente de Acuíferos Vivos, datos a la mano, trazará un
cuadro de la situación del agua y los acuíferos en la provincia,
ilustrando las gravísimas consecuencias de la sobreexplotación y la
falta de ordenación por parte de las administraciones.

A continuación, <!-- more --> Jorge Blanco Abad, activista almeriense
por el agua y recién incorporado al proyecto Sunseed Tecnología del
Desierto, se centrará en la situación del Río Aguas de Sorbas, objeto de
un verdadero expolio por parte de empresas inversoras en la agricultura
súper-intensiva.

Esperamos que a raíz de estas primeras intervenciones se origine un
debate serio y enriquecedor para todas.

Además, estará presente la distribuidora almeriense Ediciones y
Distribuciones Fantasma con libros y folletos sobre la lucha por el
agua y otras luchas hermanas, así como la miniexposición de la fotógrafa
almeriense Anne Agüero «Fighting for Survival» sobre la aldea ecológica
de los Molinos del Río Agua, amenazada por la sobreexplotación del
Acuífero.

Para terminar, habrá un riquísimo cenador preparado por
<abbr title="Confederación Nacional del Trabajo">CNT</abbr> Almería.

¡No podéis faltar!
