Title: CNT hace un balance negativo de la desconvocatoria de huelga en el sector del manipulado almeriense
Date: 2016-03-24 09:43
Category: Opinión
Tags: Almería, C.N.T., huelga
Image: images/2016/huelga-sector-manipulado.jpg

Para CNT el acuerdo rubricado en el SERCLA queda muy lejos de la
plataforma reivindicativa con la que CCOO y UGT habían movilizado a las
trabajadoras y trabajadores del sector. Según afirmaban estas centrales
sindicales los salarios habían perdido un 7,5% desde el 2007. El acuerdo
en materia salarial contempla un incremento salarial desde el 1 de marzo
de 2016 hasta el 31 de agosto de 2017 de un 2,5% es decir 15 céntimos
por hora.

<!-- more -->

Desde el 1 de septiembre de 2017 al 31 de agosto de 2018 un 1% más, o lo
que es lo mismo, 6 céntimos. En esas fechas las envasadoras estarán
cobrando 6,50 € la hora en bruto, incluido pagas extras y vacaciones.
Para la central anarcosindicalista CNT es obvio que esta subida salarial
queda muy por debajo de lo planteado inicialmente en la tabla
reivindicativa, ni siquiera va a permitir recuperar poder adquisitivo ya
que no contempla, las variaciones al alza que pueda experimentar el IPC
en este periodo. 

Otro de los puntos flacos del acuerdo es la reducción del porcentaje de
eventuales en las plantillas de las empresas. Del 15% como máximo de
personal temporal que se reivindicaba inicialmente se ha terminado
firmando un porcentaje del 40% para la campaña 2017-2018, casi la mitad
de la plantilla podrá seguir siendo contratada bajo la modalidad del
contrato de fin de obra o servicio.

En cuanto a las horas de trabajo se vuelve a pactar una jornada más amplia
que la ordinaria que está establecida en 40 horas semanales según el ET
y que, inicialmente, era otro de los ejes de la plataforma
reivindicativa para mejorar las condiciones de trabajo. Este pacto
establece una jornada semanal de 48 horas a partir de septiembre de 2016
reduciendo, en 2 horas, lo pactado en el convenio anterior. La
posibilidad de reducción de la jornada a 45 horas por conciliación
familiar para las trabajadoras o trabajadores que lo soliciten demuestra
que, quienes han rubricado el acuerdo, no tienen ni idea de lo que es la
conciliación laboral y familiar. Donde quizás haya alguna mejoría es en
la garantía de 160 horas mensuales de trabajo mínimo en cómputo
trimestral para cuando empiece a decaer la campaña, garantía que en la
práctica, no disfrutarán los contratados por fin de obra o servicio
porque paulatinamente, se les comunicará su fin de contrato. Como algo
novedoso, el acuerdo contempla la supervisión de la Consejería de Empleo
para asegurar el cumplimiento del convenio en todas las empresas del
sector cuestión que, creemos, es un brindis al sol, quien tiene las
competencias para hacer cumplir lo pactado en convenio es la Inspección
de Trabajo y la acción sindical de los trabajadoras y trabajadores. El
acuerdo deja en el tintero otras cuestiones como los 30 minutos de
bocadillo, la obligatoriedad de poner medios de transporte a las
plantillas, cobrar como horas extras la hora que sobrepase la jornada
ordinaria, 12 horas de descanso entre jornadas etc. Tal y como
comentábamos en el comunicado de apoyo a esta huelga remitido a la
prensa el viernes 18 de marzo, la central anarcosindicalista CNT abogaba
por un proceso asambleario donde los trabajadores y trabajadoras
decidieran sobre su futuro, si ratificar acuerdos o continuar con la
lucha. Los sindicatos llamados mayoritarios no han ido más allá de la
"asamblea" de representantes unitarios y tan siquiera por unanimidad han
aceptado el acuerdo. 

Por último, desde CNT destacamos la falta de rigor de algunos medios de
comunicación, concretamente, La Voz de Almería, que a la 1:35 de la
madrugada del día 19 ya publicaba en su versión digital, que se había
desconvocado la huelga. Desde CNT nos preguntamos si esta celeridad por
confirmar el fin de la convocatoria de huelga, pendiente en esos
momentos de ser ratificada por los representantes unitarios, está
relacionada con la cantidad de páginas que la patronal Coexphal y sus
asociados, ocupan frecuentemente en este medio.

Pese a estos reveses, la CNT anima a todos los trabajadoras y
trabajadores a organizarse al margen del sindicalismo oficial que los ha
defraudado. Los pocos derechos escritos sobre el papel hay que
defenderlos ahora en las empresas. CNT propone un sindicalismo sin
subvenciones, sin liberados que alcanzan "preacuerdos" de las
condiciones de trabajo que ellos no van a padecer; sin comités, que en
algunos almacenes están alineados con los intereses de las empresas. Se
puede desarrollar acción sindical desde otros ámbitos, como por ejemplo,
las secciones sindicales que propugna CNT.
