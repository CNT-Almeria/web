Title: Concentración antifascista: 16 de agosto
Date: 2017-08-15 00:09
Tags: Almería, antifascismo, concentracion, feria, fiestas
Category: Eventos
Slug: concentracion-antifascista-16-agosto-2017
Image: images/2017/concentracion-antifascista-cartel.jpeg

Cada año se vienen sucendiendo agresiones durante la feria, ante la
pasividad de una parte de la sociedad. Hace dos años fue agredida una
pareja gay de forma brutal después de haber sido llamados «maricones».
Ante las recientes agresiones fascistas en otras partes del mundo
tampoco podemos permanecer impasibles.

Con este panorama decidimos salir a las calles: el 16 de agosto a las
<time datetime="20:00">20:00h</time> en la Puerta de Puchena
realizaremos una concentración bajo el lema de «por una feria libre de
agresiones xenófobas y patriarcales».

No queremos más agresiones fascistas. No les daremos tregua;
permaneceremos en la lucha junto a las organizaciones que nos
acompañarán en la concentración. Solidaridad y apoyo a las víctimas y
familiares de agresiones fascistas. ¡Combate al fascismo!
