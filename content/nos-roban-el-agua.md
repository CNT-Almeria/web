Title: Nos roban el agua
Date: 2017-11-02 12:51
Tags: agua, agricultura, expolio, manifestación, música
Category: Eventos
Image: images/2017/mani-por-el-agua.jpg

El <time datetime="2017-11-04T19:00">sábado 4 de noviembre a las
19:00h</time> nos manifestaremos en Puerta Puchena junto a otros
colectivos por la defensa del agua.

La situación es crítica, tal y como se explicó en la charla-debate *[Robo
del agua: Acuafundios en
Almería](http://www.cntaitalmeria.es/robo-del-agua-acuafundios-en-almeria.html)*.
Empresas de agricultura intensiva quieren gastar el agua de todas para
obtener un beneficio económico. Esto plantea un gran problema para
pequeñas agricultoras y habitantes de las zonas más afectadas, sobre
todo el Acuífero del Alto Aguas, [el más sobreexplotado de toda
Andalucía](http://www.almeriahoy.com/2017/03/denuncian-que-se-esta-extrayendo-un-300.html).
Además, de los devastadores efectos laborales, este expolio del agua
afecta gravemente al Paraje Natural del Karst en Yeso de Sorbas.

¡No podemos permitirlo!
