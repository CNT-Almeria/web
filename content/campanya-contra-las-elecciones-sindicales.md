Title: Campaña contra las elecciones sindicales
Date: 2014-12-01
Tags: campañas, destacado
Category: Noticias
Slug: campanya-contra-las-elecciones-sindicales
Image: images/2014/propaganda-contra-elecciones-sindicales.jpg

CNT-AIT Almería ha llevado a cabo una campaña en contra de las
elecciones sindicales por distintas empresas de la zona, sobre todo
relacionadas con el comercio, a través de carteles y dípticos
informativos.

Como es habitual, otra vez nos piden que vayamos a votar en las
elecciones sindicales. El modelo sindical en España está diseñado para
que los/as trabajadores/as deleguen su capacidad de decisión y acción en
profesionales del sindicalismo, los/as cuales gozan de garantías frente
a la empresa, para que esta sea la única que tenga capacidad de
negociación sin rendir cuentas a ningún/a trabajador/a. <!-- more -->
Esto ha llevado por un lado a que el sindicalismo se institucionalice,
pierda toda reivindicación de contenido ideológico, y por otro lado, a
que los/as trabajadores/as se olviden de la lucha por la defensa y
promoción de nuestros intereses de clase, ya que se llega a la misma
lógica que el parlamentarismo: la política es asunto de los/as
políticos/as, y el sindicalismo es algo exclusivo de los/as
sindicalistas. Así se ha llevado a la apatía y sumisión de los/as
trabajadores/as, y a que sigamos y acatemos las directrices de las
cúpulas y directivas de los sindicatos sin cuestionarlos. Votar en las
elecciones sindicales a lo único que lleva es a que se continúe
perpetuando que los sindicatos cobren subvenciones a través del criterio
de "representación" que legitima tu voto, y que siga habiendo personas
que parasiten de tu trabajo diario y sigan diciendo que nos representan.

![Propaganda contra elecciones sindicales](http://www.cntaitalmeria.es/images/2014/propaganda-contra-elecciones-sindicales-2.jpg)

Esta campaña también pretende denunciar la constantemente la corrupción
dentro de los sindicatos tanto a nivel estatal como a nivel de secciones
sindicales, cómo tantos/as oportunistas realizan carrera profesional
para vivir bien a costa de traicionar y vender a los/as demás, y cómo
tanto las empresas como el estado nos niegan la libertad sindical para
que nuestra denuncia y nuestra voz sea anulada o marginada.

**Rompe el cerco, no votes, organízate y lucha**
