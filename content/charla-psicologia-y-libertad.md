Title: Psicología y libertad II: Actos responsables
Date: 2017-10-30 20:48
Tags: cenador, libertad, psicología
Category: Eventos
Slug: charla-psicologia-y-libertad
Image: images/2017/psicologia-y-libertad.jpg

El <time datetime="2017-11-02T20:00">jueves 2 de noviembre de 2017 a las
20:00h</time> tendrá lugar una charla sobre psicología y libertad en el local
de <abbr title="Confederación Nacional del Trabajo">CNT</abbr>. El ponente es
Javier Vela Pérez (psicólogo). Después del coloquio habrá un cenador.
