Title: Crónica de la manifestación del 1º de mayo
Date: 2013-05-02 18:29
Category: Noticias
Tags: 1º de mayo, Adra, Almería, laboral
Image: images/2013/cronica-de-la-manifestacion-del-1-de-mayo-1.jpg

La CNT-AIT de Almería y Adra han salido a la calle también este 1º de mayo por
la capital almeriense. Bajo el lema: “Rompamos con su juego. Construyamos la
alternativa”, la manifestación ha partido del barrio de los Ángeles y
trascurrido por otros como el Barrio Alto, Altamira o Avenida de la Estación,
sin ningún tipo de incidentes.

<!-- more -->

Terminada la manifestación nos hemos dirigido hacia la Carpa Juan Goytisolo
para celebrar un mitin-fiesta.

Durante todo el recorrido se oyeron canticos en contra de la situación actual,
las políticas de recortes llevadas a cabo por el gobierno y en contra del
sindicalismo institucionalizado (CCOO-UGT) complice de la misma.

La CNT-AIT ha querido estar presente en este día símbolo de la dureza de los
ataques que la clase trabajadora ha sufrido a lo largo de la historia y sigue
sufriendo, pero también de nuestra resistencia y de nuestra lucha por nuestra
emancipación, de nuestra capacidad de organizarnos y de transformar esta
sociedad.

![Imagen de la CNT-AIT en manifestación del 1 de mayo](http://www.cntaitalmeria.es/images/2013/cronica-de-la-manifestacion-del-1-de-mayo-2.jpg)
![Imagen de la CNT-AIT en manifestación del 1 de mayo](http://www.cntaitalmeria.es/images/2013/cronica-de-la-manifestacion-del-1-de-mayo-3.jpg)
![Imagen de la CNT-AIT en manifestación del 1 de mayo](http://www.cntaitalmeria.es/images/2013/cronica-de-la-manifestacion-del-1-de-mayo-4.jpg)
