Title: Cineforum feminista 2: Lisístrata
Date: 2017-11-27 16:56
Tags: antipatriarcal, cine, cineforum, discusión, feminismo, Lisístrata
Category: Eventos
Image: images/2017/cartel-cineforum-lisistrata.jpg

Este miércoles 29 de noviembre a las 20:00 proyectaremos la película
*Lisístrata* en el local de CNT-AIT Almería,
como continuación a nuestro ciclo de cine antipatriarcal.

Como en [la sesión
anterior](http://www.cntaitalmeria.es/cineforum-feminista-1-las-memorias-de-antonia.html),
invitamos a palomitas. Sobre todo, venid con ganas de disfrutar y de
debatir y charlar tras la película.
