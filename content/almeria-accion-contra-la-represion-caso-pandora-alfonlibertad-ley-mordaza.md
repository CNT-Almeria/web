Title: Acción contra la represión (Caso Pandora, Alfonlibertad, Ley Mordaza...)
Date: 2015-01-28 16:25
Category: Opinión
Tags: Almería, represión, vídeo

Acción llevada a cabo por CNT-AIT Almería en contra de la "Ley Mordaza"
y en solidaridad con los últimos casos de represión y criminalización al
movimiento libertario y la clase trabajadora.

Nada nuevo bajo el cielo. Han precarizado nuestra vida hasta
flexibilizar insoportablemente nuestra cotidianidad que a poco que se
manifieste una mínima respuesta, solo se les ocurre atacarnos
frontalmente. Nosotros, sabemos que se debe al miedo. Que es el miedo
que nos tienen, lo que les motiva a actuar así. Es una vieja receta. Como
lo es el liberalizarlo todo.

<!-- more -->

Pero no nos confundamos, la Ley Mordaza es una tentativa de aniquilación
aún mayor que todo tipo de desobediencia. Quieren legislar y por ende
condenar todo lo condenable. Acuerdo de máximos diríamos. Nosotros,
queremos preguntar: ¿algo estarán haciendo mal cuando la única solución
que plantean es la sanción y el castigo? Sabemos de sobra que no van a
parar hasta que todos los contratos sean por minutos, no existan
despidos, la movilidad sea de miles de kilómetros, todo el tinglado
público este desmantelado y un largo etcétera, pero el problema es que
cada día ese nosotros que conoce lo que están haciendo es mayor. Por eso
decimos: NO a la "Ley Mordaza", ¡¡el que mucho miedo tiene, mucho malo
ha hecho!!

La represión ha tomado nombre, entre otros, y como siempre, en lo
libertario, lo anarquista. Los anárquicos teníamos que recibir nuestra
ración de represión brutal y de criminalización. Ya tocaba. Cada pocos
meses algunas compañeras y compañeros deben ser vapuleados y privados de
libertad. Nuestra idea pisoteada. Al grito: “que vienen los
anarquistas”, han montado la enésima estratagema contra nosotros/as.
Pero
el [#yotambiénsoyanarquista](https://www.facebook.com/hashtag/yotambi%C3%A9nsoyanarquista?source=feed_text&story_id=326106674248034) del
“Caso Pandora” se escuchará cien mil veces más y con más fuerza. El
efecto Pandora es el efecto anárquico.

También, destacamos una persecución infame, una investigación que solo
demuestra la falta de pudor, que acaba con el encarcelamiento de Alfon,
con un juicio farsa y la criminalización de él y por extensión de toda
la clase obrera que representa el compañero.

¡Basta de juicios farsa! ¡Basta de persecución a la clase obrera! ¡Alfon
libertad!
