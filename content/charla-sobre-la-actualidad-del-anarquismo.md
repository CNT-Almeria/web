Title: Charla sobre la actualidad del arnarquismo
Date: 2008-06-21 19:30
Category: Eventos
Tags: charla

El próximo sábado día 21 de junio a las 19:30 tendrá lugar una
conferencia-charla-debate impartida por Alejandro (CNT-Madrid) sobre la
actualidad del anarquismo y sus vías y posibilidades de lucha.

Esta tendrá lugar en el salón de actos de la CNT en Almería,
C/Javier Sanz, 14, 3ª planta,

Esperamos tu asistencia y tu participación.
